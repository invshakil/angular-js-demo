# Angular 2

## Description
This is a learning project of angular 2 js where we are trying to work on different contacts. We will try to do edit, deletion of contacts too.

## Usage
Follow the following steps and you're good to go! Important: Typescript and npm has to be installed on your machine!

1: Clone repo
```
git@bitbucket.org:invshakil/angular-js-demo.git```
2: Install packages
```
npm install
```
3: Start server (includes auto refreshing) and gulp watcher
```
npm start
```
