/**
 * Created by shaki on 21-Apr-17.
 */
import {Contact} from "./contact";

export const CONTACTS: Contact[] = [
    {firstName: "Alex", lastName: "Arnold", phone: "01752135625", email: "alex@gmail.com"},
    {firstName: "Bobby", lastName: "Harold", phone: "01442135625", email: "bobby@gmail.com"},
    {firstName: "Harry", lastName: "Rednep", phone: "01541815287", email: "harry@gmail.com"},
    {firstName: "Sunny", lastName: "Deole", phone: "06485684659", email: "sunny@gmail.com"}
]