/**
 * Created by shaki on 21-Apr-17.
 */

export interface Contact{
    firstName: string,
    lastName: string,
    phone: string,
    email: string
}