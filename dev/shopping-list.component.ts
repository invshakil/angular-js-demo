import {Component} from "angular2/core";

@Component ({
    selector: 'shopping-list',
    //language=Angular2HTML
    template: `
        <ul>
            <li 
                *ngFor="#shoppingListItems of shoppingListItems"
                (click)="onItemClicked(shoppingListItem)"
            >
                {{  shoppingListItems.name  }}
            </li>
        </ul>
        <input type="text" [(ngModel)]="selectedItem.name">
        <input type="text" #shoppingListItem>
        <button (click)="onAddItem(shoppingListItem)">Add Item</button>
    `
})

export class ShoppingListComponent{
    public shoppingListItems = [
        {name: "Milk"},
        {name: "Sugar"},
        {name: "Bread"}

    ];
    public selectedItem = {name: ""};

    onItemClicked(shoppingListItem){
        this.selectedItem = shoppingListItem;
    }
    onAddItem(shoppingListItem){
        this.shoppingListItems.push({name: shoppingListItem.value});
    }
}